<!DOCTYPE html>
<html>
<head>
    <title>resume</title>
    <?PHP header("Content-Type: text/html; charset=utf-8");?>
    <meta http-equiv="content-type" content="text/html" charset="UTF-8" />
    <link rel="stylesheet" href="./style.css" media="screen" />
    <meta name=viewport content="width=device-width, initial-scale=1">
</head>
<body>
<div class="logogo"></div>
<div id="general">

    <div class="inner-left">
        <h4>
        <div class="block">
            <center>
            <a href="http://resume.gelort.com/img/photo.jpg" target="_blank"><img id="photo" src="./img/photo.jpg"></a>
                <!-- <img id="photo" src="./img/photo.jpg"> -->
            </center>
        </div>
        <div class="block">
        <!-- <h1>RESUME</h1> -->
            <center>Обо мне </center>
            <hr>
            <p>Шурухин Владимир Павлович <br> 
            <p>Дата рождения:  19.11.1989г<br>
            <p>Семейное положение: не женат, детей нет <br>
            <p>Место жительства: г.Челябинск
        </div>
        <div class="block">
            <center>Контакты</center>
            <hr>
            Телефон: <a href="tel:+79128088939">+79128088939</a><br>
            E-mail: <a href="mailto:iccid_zxz@mail.ru" title="iccid_zxz@mail.ru">iccid_zxz@mail.ru</a><br>
            Сайт: <a href="http://www.gelort.com/" target="_blank">www.gelort.com</a>
        </div>
        <div class="block">
            <center>Личные качества</center>
            <hr>
            Быстрая обучаемость,<br> 
            Целеустремленность,<br> 
            Работоспособность,<br> 
            Коммуникабельность,<br> 
            Умение работать в команде.
        </div>
        <div class="block">
            <center>Умения</center>
            <hr>
            <div class="progress-bar green2 stripes">
                <span style="width: 19%"><div class="skills">yii2</div></span>
            </div>
            <div class="progress-bar green2 stripes">
                <span style="width: 42%"><div class="skills">php</div></span>
            </div>
            <div class="progress-bar green2 stripes">
                <span style="width: 43%"><div class="skills">css</div></span>
            </div>
            <div class="progress-bar green2 stripes">
                <span style="width: 37%"><div class="skills">mysql</div></span>
            </div>
            <div class="progress-bar green2 stripes">
                <span style="width: 28%"><div class="skills">jquery</div></span>
            </div>

        </div >
        </h4>
    </div>

    <div class="inner-right">
        <h4>
        <div class="block">
            <center>Образование</center>
            <hr>
            ВОШ №13 Среднее полное  ( г. Чебаркуль. ) <br>
            Компьютерные курсы: Настройка, ремонт и обслуживание ПК.  2004г <br>
            Наличие водительского удостоверения категории B,C.  (Стаж с января 2011) <br>
        </div>
        <div class="block">
            <center>Опыт работы</center>
            <hr>
            <p>ООО«Строительный Двор» г.Чебаркуль. Администратор программного обеспечения. (август 2007 - октябрь  2009)</p>
            <p>ООО Интертрей, г Чебаркуль. Сотрудник технической поддержки. (ноябрь 2007 - декабрь 2008);</p>
            <p><a href="http://www.chel.mts.ru/dom/" target="_blank">ОАО «МТС»</a> г. Челябинск. Монтажник ЛВС (сентябрь 2012 — февраль 2013)</p>
            <p><a href="https://www.is74.ru/" target="_blank">ООО «Интерсвязь»</a> г.Челябинск. Оператор   Call-центра. (май 2014 — апрель 2015)</p>
            <p><a href="http://itlogic.pro/" target="_blank">Itlogic.pro</a> г.Челябинск. Инженер-программист  sip-телефонии (VoIP) (май 2016 - июль 2018)</p>
            <p><a href="https://www.is74.ru/" target="_blank">ООО «Интерсвязь»</a> г.Челябинск. Инженер-программист, группа Web-разработки (июль 2018)</p>
        </div>
        <div class="block">
            <center>Специальные навыки</center>
            <hr>
            Владение компьютером – на уровне администратора, знание ОС Windows, знание ОС Linux. <br>
            Знание PHP, MySQL, CSS, (git, yii2, JavaScript, Jquery , AJAX, Bootstrap, Perl) <br>
            Английский язык: технический со словарем. <br>
        </div>

        <div class="block">
            <center>Примеры</center>
            <hr>
            1. <a href="https://gitlab.com/IssID/js_node_socket" target="_blank">https://gitlab.com/IssID/js_node_socket (работа с сокетами на node.js)</a><br>
            2. <a href="https://gitlab.com/IssID/walkmap" target="_blank">https://gitlab.com/IssID/walkmap (передвижение персоонажа по карте.) phaser.io</a><br>
            3. <a href="https://gitlab.com/IssID/Match3" target="_blank">https://gitlab.com/IssID/Match3 (три в ряд на pixiJS)</a><br>
        </div>
        </h4>
    </div>

</div>
</body>
</html>
